from django.contrib import admin
from django.urls import path, include

from PersonAPI.viewsAPI import PersonCreateApiView, GetPersonApiView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('people/', PersonCreateApiView.as_view(), name='person_api'),
    path('people/<str:iin>/', GetPersonApiView.as_view())
    ]
