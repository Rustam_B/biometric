# Тестовое задание для вакансии:
https://hh.kz/applicant/vacancy_response?vacancyId=69306140&hhtmFrom=vacancy



## Порядок установки

Склонировать удаленный репозиторий:
https://gitlab.com/Rustam_B/biometric

Установить зависимости pip install -r requirements.txt (предварительно активировал виртуальное окружение)

Описание эндпоинтов:

people/ - POST запрос с указанием ИИН

people/iin/ - GET запрос
