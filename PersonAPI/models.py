from django.db import models
from django.core.validators import MinLengthValidator

class Person(models.Model):
    iin = models.CharField(max_length=12, validators=[MinLengthValidator(12)] ,null=False, blank=False, verbose_name='ИИН')

    def __str__(self):
        return self.iin