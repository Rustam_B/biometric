from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from PersonAPI.models import Person

class PersonSerializer(serializers.ModelSerializer):

    class Meta:
        model = Person
        fields = '__all__'