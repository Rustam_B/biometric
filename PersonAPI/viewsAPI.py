import datetime
from rest_framework import generics, status
from rest_framework.response import Response

from PersonAPI.serializers import PersonSerializer

now = datetime.datetime.now() #текуший год

class PersonCreateApiView(generics.CreateAPIView):
    serializer_class = PersonSerializer

    def get_age(self, request, *args, **kwargs):
        now = datetime.datetime.now().year
        if request.data.get('iin')[0] == '0':
            age = '20' + request.data.get('iin')[0:2]
            total = now - int(age)
        else:
            age = '19' + request.data.get('iin')[0:2]
            total = now - int(age)
        return total

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        data = {
            'iin': request.data.get('iin'),
            'age': self.get_age(request=request),
            }
        return Response(status=status.HTTP_201_CREATED, data=data)

class GetPersonApiView(generics.ListAPIView):
    serializer_class = PersonSerializer

    def get_age(self, request, *args, **kwargs):
        now = datetime.datetime.now().year
        if self.kwargs['iin'][0] == '0':
            age = '20' + self.kwargs['iin'][0:2]
            total = now - int(age)
        else:
            age = '19' + self.kwargs['iin'][0:2]
            total = now - int(age)
        return total

    def list(self, request, *args, **kwargs):
        data = {
            'iin': self.kwargs['iin'],
            'age': self.get_age(request),
            }
        return Response(data=data)